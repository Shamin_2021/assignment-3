#include <stdio.h>
#include <stdlib.h>

int main()
{
    float number;
    printf("enter your number  :");
    scanf("%f", &number);

    if(number>0){
        printf("number is a positive number");
    }
    else if (number== 0){
        printf("number is a zero");
    }
    else{
        printf("number is a negative number");
    }

    return 0;
}
