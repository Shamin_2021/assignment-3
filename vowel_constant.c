#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(void) {
  char letter;
    char vowels[5]={'a','e','i','o','u'};

    printf("enter your letter    :");
    scanf("%c", &letter);
    letter=tolower(letter);

    int i;
   char flag[10];

    if(letter >='a' && letter  <='z' ){
      for(i=0;i<5;i++){
        if(letter == vowels[i]){
            strcpy(flag,"vowel");
        }
        else{
            strcpy(flag,"constant");
        }}
      printf("\n the letter : %c is a %s  ",letter,flag);
    }
    else{
      printf("the entered character is not a letter");
    }

  //second method
  char letter;
  printf("enter your letter : ");
  scanf("%c",&letter);

  if(letter == 'A' || letter == 'E' ||letter == 'I' ||letter == 'O' ||letter == 'U' ||letter == 'a' ||letter == 'e' ||letter == 'i' ||letter == 'o' ||letter == 'u' ){
    printf("letter is vowel");

  }
  else{
    printf("letter is constant");
  }
    return 0;
}
